﻿using EFMigrationsPoC;
using EFMigrationsPoC.Model;
using System;

namespace EFMigrationPoCApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using var db = new EFMigrationsPoCDb();

            var clientObject1 = new ClientObject { Name = "Name 1" };
            var field1 = new Field { Name = "Field 1" };
            var field2 = new Field { Name = "Field 2" };

            var clientObject2 = new ClientObject { Name = "Name 2" };
            var field3 = new Field { Name = "Field 3" };


            var clientObjectField11 = new ClientObjectField
            {
                ClientObject = clientObject1,
                Field = field1
            };
            var clientObjectField12 = new ClientObjectField
            {
                ClientObject = clientObject1,
                Field = field2
            };
            var clientObjectField21 = new ClientObjectField
            {
                ClientObject = clientObject2,
                Field = field1
            };
            var clientObjectField23 = new ClientObjectField
            {
                ClientObject = clientObject2,
                Field = field3
            };
            db.Fields.AddRange(new [] { field1, field2,field3 });
            db.ClientObjects.AddRange(new [] { clientObject1, clientObject2 });
            db.ClientObjectFields.AddRange(new [] { clientObjectField11, clientObjectField12, clientObjectField21, clientObjectField23 });
            db.SaveChanges();
        }
    }
}
