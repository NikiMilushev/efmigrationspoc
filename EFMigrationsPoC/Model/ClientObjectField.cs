﻿namespace EFMigrationsPoC.Model
{
    public class ClientObjectField
    {
        public int FieldId { get; set; }
        public int ClientObjectId { get; set; }
        public ClientObject ClientObject { get; set; }
        public Field Field { get; set; }

    }
}