﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EFMigrationsPoC.Model
{
    public class Field
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public ICollection<ClientObjectField> ClientObjectFields { get; set; }
    }
}
