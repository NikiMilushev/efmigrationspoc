﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFMigrationsPoC.Model
{
    public class ClientObject
    {
        public int Id { get; set; }
        
        [MaxLength(50)]
        public string Name { get; set; }
        public ICollection<ClientObjectField> ClientObjectFields { get; set; }
    }
}