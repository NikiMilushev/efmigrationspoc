﻿// <auto-generated />
using EFMigrationsPoC;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EFMigrationsPoC.Migrations
{
    [DbContext(typeof(EFMigrationsPoCDb))]
    [Migration("20191001201932_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EFMigrationsPoC.Model.ClientObject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("ClientObjects");
                });

            modelBuilder.Entity("EFMigrationsPoC.Model.ClientObjectField", b =>
                {
                    b.Property<int>("FieldId")
                        .HasColumnType("int");

                    b.Property<int>("ClientObjectId")
                        .HasColumnType("int");

                    b.HasKey("FieldId", "ClientObjectId");

                    b.HasIndex("ClientObjectId");

                    b.ToTable("ClientObjectFields");
                });

            modelBuilder.Entity("EFMigrationsPoC.Model.Field", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Fields");
                });

            modelBuilder.Entity("EFMigrationsPoC.Model.ClientObjectField", b =>
                {
                    b.HasOne("EFMigrationsPoC.Model.ClientObject", "ClientObject")
                        .WithMany("ClientObjectFields")
                        .HasForeignKey("ClientObjectId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.HasOne("EFMigrationsPoC.Model.Field", "Field")
                        .WithMany("ClientObjectFields")
                        .HasForeignKey("FieldId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
