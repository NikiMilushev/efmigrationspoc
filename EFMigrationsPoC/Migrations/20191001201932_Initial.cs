﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFMigrationsPoC.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientObjects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientObjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Fields",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fields", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientObjectFields",
                columns: table => new
                {
                    FieldId = table.Column<int>(nullable: false),
                    ClientObjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientObjectFields", x => new { x.FieldId, x.ClientObjectId });
                    table.ForeignKey(
                        name: "FK_ClientObjectFields_ClientObjects_ClientObjectId",
                        column: x => x.ClientObjectId,
                        principalTable: "ClientObjects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ClientObjectFields_Fields_FieldId",
                        column: x => x.FieldId,
                        principalTable: "Fields",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientObjectFields_ClientObjectId",
                table: "ClientObjectFields",
                column: "ClientObjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientObjectFields");

            migrationBuilder.DropTable(
                name: "ClientObjects");

            migrationBuilder.DropTable(
                name: "Fields");
        }
    }
}
