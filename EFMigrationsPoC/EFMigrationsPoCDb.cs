﻿using EFMigrationsPoC.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace EFMigrationsPoC
{
    public class EFMigrationsPoCDb : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder options)
                    => options.UseSqlServer("Server=(LocalDb)\\MSSQLLocalDB;Database=EFMigrationsPoC;Trusted_Connection=True");
        public DbSet<Field> Fields { get; set; }
        public DbSet<ClientObject> ClientObjects{ get; set;}
        public DbSet<ClientObjectField> ClientObjectFields{ get; set;}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<ClientObjectField>()
                .HasOne(c => c.ClientObject)
                .WithMany(c => c.ClientObjectFields)
                .HasForeignKey(c => c.ClientObjectId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder
                .Entity<ClientObjectField>()
                .HasOne(c => c.Field)
                .WithMany(c => c.ClientObjectFields)
                .HasForeignKey(c => c.FieldId)
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<ClientObjectField>()
                .HasKey(x => new { x.FieldId, x.ClientObjectId });
        }
    }
}
